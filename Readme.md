# gtimer

A simple ruby script for checking http response time averages over a given period of seconds.

```
Usage: ruby gtimer.rb <seconds to run>
```

__Example:__
```
# Specify the seconds you want to the script to run
$ ./gtimer.rb 5

Checking response times for https://gitlab.com over 5s
--
Avgerate Response Time over 5s: [0.38s]
Number of Requests: [13]
Requests Per Second: [2]
--

```
