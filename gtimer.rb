#!/usr/local/bin/ruby

require 'net/http'
require "benchmark"


class Realtime

   def initialize(url, length)
      @url=url
      @length=length.to_i # in minutes
      @times = []
      @sum = 0
      puts "Checking response times for #{@url} over #{@length}s\n--\n"
   end

   def Get()
     url = URI(@url)
     http           = Net::HTTP.new(url.host, url.port)
     http.use_ssl = true
     res            = http.get(url)
     return res.body
   end

   def Check()
     return Benchmark.realtime do
       Get()
     end
   end

   def Run()
    #  Run http requests on a separate thread
     check_thr = Thread.new {
        while true do
          c = Check()
          @times << c
        end
     }

    #  Wait for given length of time
     sleep @length
     check_thr.exit
     @sum = 0
     @times.each do |i|
        @sum += i
      end

     avg = (@sum/@times.length).round(2)
     $l = +1

     puts "Avgerate Response Time over #{@length}s: [#{avg}s]\nNumber of Requests: [#{@times.length}]\nRequests Per Second: [#{@times.length/@length}]\n--"
     return
   end

end

if ARGV.length < 1 then
  puts "Usage: ./#{$0} <seconds to run>"
else
  Realtime.new("https://gitlab.com", ARGV[0]).Run()
end
